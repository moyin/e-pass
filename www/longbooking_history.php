<?php
session_start();
include("includes/connections.php");
include("includes/functions.php");
 chkLogin();
     
     $id =  $_SESSION['student_id'] ;
   //  var_dump($id);
     
?>




<!DOCTYPE html>
<html>
<head>
	<title>booking history</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="booking-history">

<!-- BACKGROUND IMAGE AND OVERLAY -->
	<div class="background">
		<div class="overlay"></div>
		<div class="img"></div>
	</div>

<!-- TOP BAR WITH LOGO AND TEXT -->
	<div class="top-bar">
		<div class="logo"></div>
		<h1>Long Distance Booking History</h1>
	</div>


<!-- FLEXIBLE MENU BUTTON -->
	<div class="menu-btn">
		<div class="b-bars b1"></div>
		<div class="b-bars b2"></div>
		<div class="b-bars b3"></div>
	</div>

<!-- MODAL MENU FOR NAVIGATION -->
	<div class="menu-modal">
		<div class="dialog">
			<button class="def-button trigger-btn">What do you want to do?</button>
			<ul class="options-list">
				<a href="long_distance.php"><li class="option">Long Distance Booking</li></a>
				<a href="short_distance.php"><li class="option">Short Distance Booking</li></a>
				<a href="shortbooking_history.php"><li class="option">Short Distance Booking History</li></a>
				<a href="longbooking_history.php"><li class="option">Long Distance Booking History</li></a>
				<a href="logout.php"><li class="option">log Out</li></a>
			</ul>
		</div>
	</div>


<!-- BOOKING HISTORY TABLE -->
	<table class="booking-history-table">
		<thead>
			<tr>
				<th class="booking-date">DATE OF BOOKING</th>
				<!-- <th class="booking-type">BOOKING TYPE</th> -->
				<th class="approval">APPROVAL</th>
				<th class="reason">REASON</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$a = longDistanceBookingHistory($conn,$id);
			echo $a;
			?>
		</tbody>
	</table>

<script type="text/javascript" src="js/pass.js"></script>
</body>
</html>
