
<?php
//include("includes/header.php");
include("includes/connections.php");
include("includes/functions.php");

$errors = [];

if(array_key_exists('register', $_POST))
{

	if(!empty($_POST['fname']))
	{
		$f = $_POST['fname'];
	}
	else
	{
		$errors['fname'] ="Please enter first name";
	}


	if(!empty($_POST['lname']))
	{
		$ln = $_POST['lname'];
	}
	else
	{
       $errors['lname'] = "Please enter last name";
	}



	if(!empty($_POST['hall']))
	{
		$h = $_POST['hall'];
	}
	else
	{
       $errors['hall'] = "Please select hall";
	}





	if(!empty($_POST['room_no']))
	{
		$r = $_POST['room_no'];
	}
	else
	{
		$errors['room_no'] = "Please select room number";
	}



	if(!empty($_POST['address']))
	{
		$a = $_POST['address'];
	}
	else
	{
		$errors['address'] = "Please select address";
	}


	if(!empty($_POST['denomination']))
	{
		$d = $_POST['denomination'];
	}
	else
	{
		$errors['denomination'] = "Please select denomination";
	}


	if(!empty($_POST['parent_no']))
	{
		$p = $_POST['parent_no'];
	}
	else
	{
		$errors['parent_no'] = "Please select parent_no";
	}

	if(!empty($_POST['course']))
	{
		$c = $_POST['course'];
	}
	else
	{
		$errors['course'] = "Please select course";
	}

	if(!empty($_POST['matric_no']))
	{
		$m = $_POST['matric_no'];
		$chk = doesMatricExist($conn,$m);
    if($chk){
      $errors['matric_no'] = "Matric number already exists";
    }
	}else
	{
		$errors['matric_no'] = "Please enter matric number";
	}

	if(!empty($_POST['level']))
	{
		$l = $_POST['level'];
	}
	else
	{
		$errors['level'] = "Please select level";
	}


	if(!empty($_POST['pword']))
	{
		if($_POST['pword'] == $_POST['password'])
		{
			$pw = hashPassword($_POST['pword']);
		}
		else
		{
			$errors['pword'] = "Passwords did not match";
		}
	}
	else
	{
		    $errors['pword'] = "Please enter password";
	}
	//echo "fuck";
	
	if(empty($errors))
	{
		registerStudent($conn,$f,$ln,$h,$r,$m,$a,$d,$p,$c,$l,$pw);
	}

}

?>
<!DOCTYPE html>
<html>
<head>
	<title>login</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="login">

<!-- BACKGROUND IMAGE AND OVERLAY -->
	<div class="background">
		<div class="overlay"></div>
		<div class="img"></div>
	</div>

<!-- TOP BAR WITH LOGO AND TEXT -->
	<div class="top-bar">
		<div class="logo"></div>
		<h1>Babcock University Pass Booking</h1>
	</div>


<!-- LOGIN FORM -->
	<form  action="login.php"   class="def-form login-form clearfix"  method="POST">
		<label for="login-form" class="header">REGISTER</label>
		<?php
    	    printError('fname',$errors);
    	?>
		<input type="text" name="fname" placeholder="First Name" class="text-field">
		<?php
    	    printError('lname',$errors);
    	?>
		<input type="text" name="lname" placeholder="Last Name" class="text-field">
		<?php
    	    printError('hall',$errors);
    	?>
		<select name="hall" class="text-field">
			<?php
           $a = getHall($conn);
           echo $a;
          
        ?>
		</select>
		<?php
    	    printError('room_no',$errors);
    	?>
		<input type="text" name="room_no" placeholder="Room Number" class="text-field">
		<?php
    	    printError('address',$errors);
    	?>
		<input type="text" name="address" placeholder="Address" class="text-field">
		<?php
    	    printError('denomination',$errors);
    	?>
		<input type="text" name="denomination" placeholder="Denomination" class="text-field">
		<?php
    	    printError('parent_no',$errors);
    	?>
		<input type="text" name="parent_no" placeholder="Parent No." class="text-field">
		<?php
    	    printError('course',$errors);
    	?>
		<input type="text" name="course" placeholder="Course" class="text-field">
		<?php
    	    printError('matric_no',$errors);
    	?>
		<input type="text" name="matric_no" placeholder="Matric No." class="text-field">
		<?php
    	    printError('level',$errors);
    	?>
		<input type="text" name="level" placeholder="Level" class="text-field">
		<?php
    	    printError('pword',$errors);
    	?>
		<input type="password" name="pword" placeholder="Password" class="text-field">
		<?php
    	    printError('password',$errors);
    	?>
		<input type="password" name="password" placeholder="Confirm Password" class="text-field">
		<input type="submit" name="register" value="Register" class="text-field">
	</form>
<?php

include("includes/footer.php");
?>
