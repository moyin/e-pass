<?php
include("includes/connections.php");

include("includes/functions.php");

$errors = []; 

# be sure user clicked the submit button 
if (array_key_exists("login", $_POST)) {

	if(!empty($_POST['fname'])){
		$fn = $_POST['fname'];
	}else{
		$errors['fname'] = "please enter the first name";
	}

	if(!empty($_POST['lname'])){
		$ln = $_POST['lname'];
	}else{
		$errors['lname'] = "please enter the last name";
	}

	if(!empty($_POST['email'])){
		$e = $_POST['email'];
		if(!filter_var($e, FILTER_VALIDATE_EMAIL))
		{
			$errors['email'] = "Invalid email address";
		}
		$chk = doesEmailExist($conn,$e);
		if($chk){
			$errors['email'] = "Email already exist";
		}

	}else{
		$errors['email'] = "please enter the email";
	}

	if(!empty($_POST['hall'])){
		$h = $_POST['hall'];
	}else{
		$errors['hall'] = "please enter the hall ";
	}

	if(!empty($_POST['password'])){
		$p = hashPassword($_POST['password']);
	}else{
		$errors['password'] = "please enter the password";
	}

	if(!empty($_POST['pass'])){
		$pa = $_POST['pass'];

		if($pa != $_POST['password']){
			$errors['pass'] = "password incorrect";
		}
	}else{
		$errors['pass'] = "please enter password again";
	}

	if(empty($errors)){
		doAdminRegister($conn,$fn,$ln,$e,$h,$p);
	}
		
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>login</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="login">

<!-- BACKGROUND IMAGE AND OVERLAY -->
	<div class="background">
		<div class="overlay"></div>
		<div class="img"></div>
	</div>

<!-- TOP BAR WITH LOGO AND TEXT -->
	<div class="top-bar">
		<div class="logo"></div>
		<h1>Babcock University Pass Booking</h1>
	</div>

<!-- LOGIN FORM -->
	<form class="def-form login-form clearfix" action="admin_login.php" method = "POST">
		<label for="login-form" class="header">Register</label>

		<?php printError('fname', $errors); ?>
		<input type="text" name="fname" placeholder="First Name" class="text-field">

		<?php printError('lname', $errors); ?>
		<input type="text" name="lname" placeholder="Last Name" class="text-field">

		<?php printError('email', $errors); ?>
		<input type="text" name="email" placeholder="email" class="text-field">

		<?php printError('hall', $errors); ?>
		<select name="hall" class="text-field">
			<?php
           $a = getHall($conn);
           echo $a;
          
        ?>
		</select>

		<?php printError('password', $errors); ?>
		<input type="password" name="password" placeholder="Password" class="text-field">

		<?php PrintError('pass', $errors); ?>
		<input type="password" name="pass" placeholder="Confirm password" class="text-field">

		<input type="submit" name="login" value="Sign In" class="text-field">
	</form>

<script type="text/javascript" src="js/index.js"></script>
</body>
</html>
