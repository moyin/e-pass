/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.49-0ubuntu0.14.04.1 : Database - pass
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pass` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pass`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` mediumint(255) unsigned NOT NULL AUTO_INCREMENT,
  `hall_id` mediumint(255) unsigned NOT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

/*Table structure for table `approve_long` */

DROP TABLE IF EXISTS `approve_long`;

CREATE TABLE `approve_long` (
  `approve_long_id` mediumint(255) unsigned NOT NULL AUTO_INCREMENT,
  `l_distance_id` mediumint(255) DEFAULT NULL,
  `student_id` mediumint(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_phone` mediumint(255) DEFAULT NULL,
  `d_of_leave` varchar(255) DEFAULT NULL,
  `d_of_return` varchar(255) DEFAULT NULL,
  `d_of_booking` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`approve_long_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `approve_long` */

/*Table structure for table `hall` */

DROP TABLE IF EXISTS `hall`;

CREATE TABLE `hall` (
  `hall_id` mediumint(255) DEFAULT NULL,
  `hall_name` mediumint(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hall` */

/*Table structure for table `long_distance` */

DROP TABLE IF EXISTS `long_distance`;

CREATE TABLE `long_distance` (
  `l_distance_id` mediumint(255) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `student_id` varchar(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_phone` mediumint(255) DEFAULT NULL,
  `d_of_leave` varchar(255) DEFAULT NULL,
  `d_of_return` varchar(255) DEFAULT NULL,
  `d_of_booking` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`l_distance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `long_distance` */

insert  into `long_distance`(`l_distance_id`,`student_id`,`destination`,`guardian_name`,`guardian_phone`,`d_of_leave`,`d_of_return`,`d_of_booking`,`address`,`purpose`) values (000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001,'1','lagos','titilayo',8388607,'2017-02-28','2017-03-03','February 25, 2017, 12:51 am','2,ajana close','visitation'),(000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002,'2','lagos','titilayo',8388607,'2017-02-28','2017-03-02','February 25, 2017, 12:59 am','ajana close','visiting'),(000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003,'2','lagos','titilayo',8388607,'2017-02-28','2017-03-02','February 25, 2017, 1:02 am','ajana close','visiting'),(000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004,'2','lagos','titilayo',8388607,'2017-02-28','2017-03-02','February 25, 2017, 1:03 am','ajana close','visiting');

/*Table structure for table `short_distance` */

DROP TABLE IF EXISTS `short_distance`;

CREATE TABLE `short_distance` (
  `s_distance_id` mediumint(255) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` mediumint(255) DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_phone` mediumint(255) DEFAULT NULL,
  `d_of_booking` varchar(255) DEFAULT NULL,
  `d_of_leave` varchar(255) DEFAULT NULL,
  `d_of_return` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`s_distance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `short_distance` */

insert  into `short_distance`(`s_distance_id`,`student_id`,`destination`,`guardian_name`,`guardian_phone`,`d_of_booking`,`d_of_leave`,`d_of_return`,`address`,`purpose`) values (1,1,NULL,NULL,NULL,'February 24, 2017, 12:35 am',NULL,NULL,NULL,NULL),(2,1,NULL,NULL,NULL,'February 24, 2017, 12:35 am',NULL,NULL,NULL,NULL),(3,1,'lagos','bisola',8388607,'February 24, 2017, 1:25 am','2017-02-07','2017-02-28','ajana close','visit parents'),(4,1,'lagos','bisola',8388607,'February 24, 2017, 9:38 am','2017-02-07','2017-02-28','ajana close','visit parents'),(5,1,'lagos','bisola',8388607,'February 24, 2017, 9:39 am','2017-02-07','2017-02-28','ajana close','visit parents'),(6,2,'lagos','titilayo',8388607,'February 25, 2017, 12:57 am','2017-02-28','2017-03-03','ajana close','visiting');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `student_id` mediumint(255) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `hall` varchar(255) DEFAULT NULL,
  `room_no` varchar(255) DEFAULT NULL,
  `matric_no` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `denomination` varchar(255) DEFAULT NULL,
  `parent_no` mediumint(255) DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `student` */

insert  into `student`(`student_id`,`fname`,`lname`,`hall`,`room_no`,`matric_no`,`address`,`denomination`,`parent_no`,`course`,`level`,`password`) values (1,'Sanya','Babatunde','Rehoboth','ff20','14/1361','lagos,Nigeria','Christain',8388607,'computer Science','300','$2y$11$b955fd0881a0207b8ca68upy3Mnx96gxh/RtLMMjKqImCNokYfQsa'),(2,'James','Babalola','Rehoboth','Gf10','14/2243','adaloko ','Christian',8388607,'maths','300','$2y$11$388de0558ca2d65e3342aOL5mbCLQ1dlCCx.YCp42yLptKseHAMN6');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
