<?php
session_start();
include("includes/connections.php");
include("includes/functions.php");
  $hall  = $_SESSION['hall'] ;
  chkAdminLogin();
       
    var_dump($hall);
    

?>

<!DOCTYPE html>
<html>
<head>
	<title>booking history</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="booking-history">

<!-- BACKGROUND IMAGE AND OVERLAY -->
	<div class="background">
		<div class="overlay"></div>
		<div class="img"></div>
	</div>

<!-- TOP BAR WITH LOGO AND TEXT -->
	<div class="top-bar">
		<div class="logo"></div>
		<h1>Approved Short Distance</h1>
	</div>


<!-- FLEXIBLE MENU BUTTON -->
	<div class="menu-btn">
		<div class="b-bars b1"></div>
		<div class="b-bars b2"></div>
		<div class="b-bars b3"></div>
	</div>

<!-- MODAL MENU FOR NAVIGATION -->
	<div class="menu-modal">
		<div class="dialog">
			<button class="def-button trigger-btn">What do you want to do?</button>
			<ul class="options-list">
			<a href="approved_long.php?hall=<?php echo $hall ?>"><li class="option">Approved Long Booking</li></a>
			<a href="approved_short.php?hall=<?php echo $hall ?>"><li class="option">Approved Short Booking</li></a>
			<a href="disapproved.php"><li class="option">Disapproved Booking</li></a>
			<a href="submitted_long.php"><li class="option">Submitted Long Distance</li></a>
			<a href="submitted_short.php"><li class="option">Submitted Short Distance</li></a>
			<a href="adminlogout.php"><li class="option">Sign Out</li></a>
		
			</ul>
		</div>
	</div>


<!-- BOOKING HISTORY TABLE -->
	<table class="booking-history-table">
		<thead>
			<tr>
				<th class="booking-date">DATE OF BOOKING</th>
				<th class="booking-type">NAME</th>
				<th class="approval">MATRIC NUMBER</th>
				<th class="reason">ROOM NUMBER</th>
			</tr>
		</thead>
		<tbody>
		  <?php
          $a = getApprovedShort($conn,$hall);
          echo $a;
		  ?>  
			
		</tbody>
	</table>
	<a href="deleteapprovalshort.php"><button class="disapprove def-button ">Clear Approve</button></a>

<script type="text/javascript" src="js/pass.js"></script>
</body>
</html>
