<?php
function printError($data,$err){
	$result = array_key_exists($data,$err);
    if($result)
    {
    	echo "<span class=\"err\">".$err[$data]."</span>";
    }

   }

function registerStudent($conn,$fname,$lname,$hall,$room_no,$matric_no,$address,$denomination,$parent_no,$course,$level,$password)
{
    //$level = 2;
    $stmt = $conn->prepare("INSERT into student(hall_id,fname,lname,room_no,matric_no,address,denomination,parent_no,course,level,password) VALUES(:h,:f,:ln,:r,:m,:a,:d,:p,:c,:l,:pw)");
    $values = 
    [
        ":h" => $hall,
        ":f" => $fname,
        ":ln" => $lname,
        ":h" => $hall,
        ":r" => $room_no,
        ":m" => $matric_no,
        ":a" => $address,
        ":d" => $denomination,
        ":p" => $parent_no,
        ":c" => $course,
        ":l" => $level,
        ":pw" => $password
    ];
    $stmt->execute($values);
}

function doesMatricExist($conn,$matric)
{
    $result = false;
    $stmt = $conn->prepare("SELECT matric_no FROM student WHERE matric_no = :m");
    $stmt->bindParam(":m",$matric);
    $stmt->execute();

    $row = $stmt->rowCount();
    if($row > 0)
    {
        $result = true;
    }
    return $result;
}

function hashPassword($pword)
{
    if(CRYPT_BLOWFISH)
    {
        $salt = "$2y$11$".substr(md5(uniqid(rand(),true)),0,22);
        $hash = crypt($pword,$salt);
    }
    return $hash;
}

 function verifyHash($conn,$hash,$password){
     return crypt($password, $hash) == $hash;
    } 


 function doStudentLogin($conn,$matric,$pword)
    {
        $err_message = ""; 
        $stmt = $conn->prepare("SELECT * FROM student WHERE matric_no = :m ");
        $stmt->bindParam(":m",$matric);

        $stmt->execute();
        $num = $stmt->rowCount();

        if($num == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $hashp = $row['password'];

            $checkP = verifyPassword($pword,$hashp);
            if($checkP)
            {
                $_SESSION['student_id'] = $row['student_id'];
                $_SESSION['fullname'] = $row['fname']. ' ' . $row['lname'];

                header("Location: home.php");
            }
            else
            {
                $err_message = "Invalid password";
                header("Location: login.php?err=$err_message");
            }

        }
        else
        {
            $err_message = "Sorry, matric number is not registered";
            header("Location: login.php?err=$err_message");
        }

   }

    function chkLogin(){
    if(!isset($_SESSION['student_id'])){
    header("location:login.php");
  }
}
 function chkAdminLogin(){
    if(!isset($_SESSION['aid'])){
    header("location:admin_login.php");
  }
}


   function verifyPassword($user_input,$hash)
    {
        return crypt($user_input,$hash)==$hash;
    }

    function bookShortDistance($conn,$id,$hall_id,$d,$gn,$gp,$dl,$dr,$a,$p)
{
    $approval =0;
    //$level = 2;
    $db = date("F j, Y, g:i a");
    $stmt = $conn->prepare("INSERT into short_distance(student_id,hall_id,destination,guardian_name,guardian_phone,d_of_booking,d_of_leave,d_of_return, address,purpose,approval) VALUES(:s,:h,:d,:gn,:gp,:db,:dl,:dr,:b,:p,:a)");
    $values = 
    [
        ":s" => $id,
         ":h" => $hall_id,
        ":d" => $d,
        ":gn" => $gn,
        ":gp" => $gp,
        ":db" => $db,
        ":dl" => $dl,
        ":dr" => $dr,
        ":b" => $a,
        ":p" => $p,
        ":a" => $approval
    ];
    $stmt->execute($values);
}
   function bookLongDistance($conn,$id,$hall_id,$d,$gn,$gp,$dl,$dr,$a,$p)
{
    $approval =0;
    //$level = 2;
    $db = date("F j, Y, g:i a");
    $stmt = $conn->prepare("INSERT into long_distance(student_id,hall_id,destination,guardian_name,guardian_phone,d_of_booking,d_of_leave,d_of_return,address,purpose,approval) VALUES(:s,:h,:d,:gn,:gp,:db,:dl,:dr,:b,:p,:a)");
    $values = 
    [
        ":s" => $id,
        ":h" => $hall_id,
        ":d" => $d,
        ":gn" => $gn,
        ":gp" => $gp,
        ":db" => $db,
        ":dl" => $dl,
        ":dr" => $dr,
        ":b" => $a,
        ":p" => $p,
         ":a" => $approval

    ];
    $stmt->execute($values);
}


function findStudentDetails($conn,$id){
	$stmt =$conn->prepare("SELECT * FROM student WHERE student_id =:id");
   	$stmt->bindParam("id",$id);
   	$stmt->execute();

   $rownum=$stmt->rowCount();
   if($rownum>0){
   while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
   	return $row;
   }
}

   }
   function shortDistanceBookingHistory($conn,$id){
  $result = " ";
  $stmt=$conn ->prepare("SELECT * FROM short_distance WHERE student_id =:id");
  $stmt->bindParam(":id",$id);
  $stmt ->execute(); 
  $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $d_of_booking =$row['d_of_booking'];
    $type =$row['approval'];
    $reason =$row['reason'];
    $grant = " ";
    $reason = " ";
    if($type ==0){
      $grant = "disapproved";
      $reason = " ";
    }
    if($type == 1)
    {
      $grant = "approved";
    }
    else
    {
      $grant = "pending";

    }

    $result .=' <tr>
				<td>'. $d_of_booking .'</td>

				<td>'.$grant.'</td>
				<td>'.$reason.'</td>
			</tr>
			';




  }

return $result;

}


}
  function longDistanceBookingHistory($conn,$id){
  $result = " ";
  $stmt=$conn ->prepare("SELECT * FROM long_distance WHERE student_id =:id");
  $stmt->bindParam(":id",$id);
  $stmt ->execute(); 
  $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
     $d_of_booking =$row['d_of_booking'];
    $type =$row['approval'];
    $reason =$row['reason'];
    $grant = " ";
    $reason = " ";
    if($type ==0){
      $grant = "disapproved";
      $reason = " ";
    }
    if($type == 1)
    {
      $grant = "approved";
    }
    else
    {
      $grant = "pending";

    }

    $result .=' <tr>
        <td>'. $d_of_booking .'</td>

        <td>'.$grant.'</td>
        <td>'.$reason.'</td>
      </tr>
      ';




  }

return $result;

}


}



function deleteApproveLong($conn){
    
   $approval =1;
   $stmt = $conn->prepare("DELETE  FROM long_distance WHERE approval = :a");
   $stmt->bindParam(":a",$approval);

    $stmt ->execute();
  header("Location:approved_long.php");
}
function deleteApproveShort($conn){
    
   $approval =1;
   $stmt = $conn->prepare("DELETE  FROM short_distance WHERE approval = :a");
   $stmt->bindParam(":a",$approval);

    $stmt ->execute();
  header("Location:approved_short.php");
}
  
    function registerHall($conn,$h){
    $stmt = $conn->prepare("INSERT into hall(hall_name) VALUES(:h)");
    $values = 
    [
        ":h" => $h
        
    ];
    $stmt->execute($values);
}


function getHall($conn){
  $result =" ";
    $stmt = $conn->prepare("SELECT * FROM hall");
    $stmt->execute();

    while($row = $stmt->fetch(PDO::FETCH_ASSOC))
    {
        $hall_id =$row['hall_id'];
        $hall_name =$row['hall_name'];

        $result .="<option value=\"$hall_id\">".$hall_name."</option>";

    }

    return $result;

   }

function doAdminRegister($conn,$fname,$lname,$e,$h,$pss){
  $stmt =$conn->prepare("INSERT INTO admin(hall_id,fname,lname,email,password)
                     VALUES(:h,:fn,:ln,:e,:p)");

  $values = [
    ':h' => $h,
    ':fn' => $fname,
    ':ln' => $lname,
    ':e' => $e,
    ':p' => $pss

  ];

  $stmt->execute($values);
  $msg = "Welcome";
  echo $msg;
  //header("Location:login.php");
}
function doesEmailExist($conn, $email){
  $result = false;
  $stmt = $conn->prepare("SELECT * FROM admin WHERE email = :e");

  $stmt->bindParam(":e",$email);
  $stmt->execute();

  $num = $stmt->rowCount();
  if($num > 0){
    $result = true;
  }

  return $result;
}

    

  function doAdminLogin($conn,$e,$p){

  $stmt = $conn->prepare("SELECT * FROM admin WHERE email = :e");
  $stmt->bindParam(":e", $e);
  $stmt->execute();

  $num = $stmt->rowCount();

  if($num == 1){
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $hash  =  $row['password'];
    $_isPasswordCorrect = verifyPassword($p,$hash);

      if($_isPasswordCorrect){
          # take user to wall
      $_SESSION['aid'] = $row['admin_id'];
      $_SESSION['fullname'] = $row['fname']. ' '. $row['lname'];
       $_SESSION['hall'] = $row['hall_id'];
      

      header("Location:admin_home.php");
    }else{
      $msg = "invalid email and password";
      header("Location:admin_login.php?msg=$msg");
    }
  }else{
    $msg = "email does not exist";
    header(("Location:admin_login.php?msg=$msg"));
  }

}
function getSubmittedShort($conn,$id){
   $result = " ";
  $stmt=$conn ->prepare("SELECT * FROM short_distance WHERE hall_id =:id");
  $stmt->bindParam(":id",$id);
  $stmt ->execute(); 
  $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $lid =$row['s_distance_id'];
    $d_of_booking = $row['d_of_booking'];
    $sid =$row['student_id'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric  =   $student['matric_no'];
     $purpose =$row['purpose'];


    $result .=' <tr>
         <td><a href="studentdetailsshort.php?lid='.$lid.'&sid='.$sid.'">'.$d_of_booking.'</a></td>
         <td><a href="studentdetailsshort.php?lid='.$lid.'&sid='.$sid.'">'.$name.'</a></td>
         <td><a href="studentdetailsshort.php?lid='.$lid.'&sid='.$sid.'">'.$matric.'</a></td>
         <td><a href="studentdetailsshort.php?lid='.$lid.'&sid='.$sid.'">'.$purpose.'</a></td>
      </tr> 
         
      ';
}
return $result;
}
}

function getSubmittedLong($conn,$id){
   $result = " ";
  $stmt=$conn ->prepare("SELECT * FROM long_distance WHERE hall_id =:id");
  $stmt->bindParam(":id",$id);
  $stmt ->execute(); 
  $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $lid =$row['l_distance_id'];
    $d_of_booking = $row['d_of_booking'];
    $sid =$row['student_id'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric  =   $student['matric_no'];
     $purpose =$row['purpose'];


    $result .=' <tr>
         <td><a href="studentdetailslong.php?lid='.$lid.'&sid='.$sid.'">'.$d_of_booking.'</a></td>
         <td><a href="studentdetailslong.php?lid='.$lid.'&sid='.$sid.'">'.$name.'</a></td>
         <td><a href="studentdetailslong.php?lid='.$lid.'&sid='.$sid.'">'.$matric.'</a></td>
         <td><a href="studentdetailslong.php?lid='.$lid.'&sid='.$sid.'">'.$purpose.'</a></td>
      </tr> 
         
      ';
}
return $result;
}
}



function getStudentDetailsShort($conn,$id){
   $result = " ";
  $stmt=$conn ->prepare("SELECT * FROM short_distance WHERE s_distance_id =:sid");
  $stmt->bindParam(":sid",$id);
  $stmt ->execute(); 
  $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    
    $sid =$row['student_id'];
    $d_of_booking=$row['d_of_booking'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric  =   $student['matric_no'];
    $course  =   $student['course'];
    $level  =   $student['level'];
     $destination  =   $row['destination'];
     $purpose =$row['purpose'];
      $address  =   $row['address'];
       $guardian_phone  =   $row['guardian_phone'];


    $result .=' 

    
    <label for="name">Name:</label>
      <p class="student-name">'.$name.'</p>
      <label for="date-of-booking">Date of booking:</label>
      <p class="date-of-booking">'.$d_of_booking.'</p>
      <label for="course">Course:</label>
      <p class="course">'.$course.'</p>
      <label for="level">Level:</label>
      <p class="level">'.$level.'</p>
      
      <label for="pass-type">Pass Type:</label>
      <p class="pass-type">Long Distance</p>
      <label for="destination">Destination:</label>
      <p class="destination">'.$destination.'</p>
      <label for="address">Address:</label>
      <p class="address">'.$address.'</p>
      <label for="guardian-phone">Guardian Phone:</label>
      <p class="guardian-phone">'.$guardian_phone.'</p>
      <label for="reason">Purpose:</label>
      <p class="guardian-phone">'.$purpose.'</p>
       
      ';
}
return $result;
}
}



function getStudentDetailsLong($conn,$id){
   $result = " ";
  $stmt=$conn ->prepare("SELECT * FROM long_distance WHERE l_distance_id =:lid");
  $stmt->bindParam(":lid",$id);
  $stmt ->execute(); 
  $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    
    $sid =$row['student_id'];
    $d_of_booking=$row['d_of_booking'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric  =   $student['matric_no'];
    $course  =   $student['course'];
    $level  =   $student['level'];
     $destination  =   $row['destination'];
     $purpose =$row['purpose'];
      $address  =   $row['address'];
       $guardian_phone  =   $row['guardian_phone'];


    $result .=' 

    
    <label for="name">Name:</label>
      <p class="student-name">'.$name.'</p>
      <label for="date-of-booking">Date of booking:</label>
      <p class="date-of-booking">'.$d_of_booking.'</p>
      <label for="course">Course:</label>
      <p class="course">'.$course.'</p>
      <label for="level">Level:</label>
      <p class="level">'.$level.'</p>
      
      <label for="pass-type">Pass Type:</label>
      <p class="pass-type">Long Distance</p>
      <label for="destination">Destination:</label>
      <p class="destination">'.$destination.'</p>
      <label for="address">Address:</label>
      <p class="address">'.$address.'</p>
      <label for="guardian-phone">Guardian Phone:</label>
      <p class="guardian-phone">'.$guardian_phone.'</p>
      <label for="reason">Purpose:</label>
      <p class="guardian-phone">'.$purpose.'</p>
       
      ';
}
return $result;
}
}


    function logOut(){
    $_SESSION = array();//clear the variables
    session_destroy();//Destroy the session itself
    setcookie('PHPSESSID','', time()-3600,'/','',0, 0);
    header("location:admin_login.php");
   }
    function logOutStudent(){
    $_SESSION = array();//clear the variables
    session_destroy();//Destroy the session itself
    setcookie('PHPSESSID','', time()-3600,'/','',0, 0);
    header("location:login.php");
   }


   function approveShort($conn,$sid){
   $a =1 ;
   $stmt =$conn->prepare("UPDATE short_distance SET approval =:a WHERE s_distance_id =:sid");
   $stmt ->bindParam(":a",$a);
   $stmt ->bindParam(":sid",$sid);
   $stmt  ->execute();
   header("location:admin_home.php");

}
function approveLong($conn,$lid){
   $a =1 ;
   $stmt =$conn->prepare("UPDATE long_distance SET approval =:a WHERE l_distance_id =:lid");
   $stmt ->bindParam(":a",$a);
   $stmt ->bindParam(":lid",$lid);
   $stmt  ->execute();
  header("location:admin_home.php");

}
function dissapprove($conn,$lid){
   $a =0 ;
   $stmt =$conn->prepare("UPDATE long_distance SET approval =:a WHERE l_distance_id =:lid");
   $stmt ->bindParam(":a",$a);
   $stmt ->bindParam(":lid",$lid);
   $stmt  ->execute();
   header("location:admin_home.php");
}
  function doReason($conn,$r){
    $stmt = $conn->prepare("INSERT into long_distance(reason) VALUES(:r)");
    $values = 
    [
        ":r" => $r
        
    ];
    $stmt->execute($values);
}
function getApprovedLong($conn,$hall){
   $result = " ";
   $approval =1;
   $stmt=$conn ->prepare("SELECT * FROM long_distance WHERE hall_id =:id AND approval =:r");
   $stmt->bindParam(":id",$hall);
   $stmt->bindParam(":r",$approval);
   $stmt ->execute(); 
   $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $lid =$row['l_distance_id'];
    $d_of_booking = $row['d_of_booking'];
    $sid =$row['student_id'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric = $student['matric_no'];
    $room  =   $student['room_no'];


    $result .='  <tr>
        <td>'. $d_of_booking .'</td>

        <td>'.$name.'</td>
        <td>'.$matric.'</td>
        <td>'.$room.'</td>

      </tr>
      ';
}
return $result;
}
}
function getApprovedShort($conn,$hall){
   $result = " ";
   $approval =1;
   $stmt=$conn ->prepare("SELECT * FROM short_distance WHERE hall_id =:id AND approval =:r");
   $stmt->bindParam(":id",$hall);
   $stmt->bindParam(":r",$approval);
   $stmt ->execute(); 
   $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $lid =$row['s_distance_id'];
    $d_of_booking = $row['d_of_booking'];
    $sid =$row['student_id'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric = $student['matric_no'];
    $room  =   $student['room_no'];


    $result .='  <tr>
        <td>'. $d_of_booking .'</td>

        <td>'.$name.'</td>
        <td>'.$matric.'</td>
        <td>'.$room.'</td>

      </tr>
      ';
}
return $result;
}
}
function getDisapprovedShort($conn,$hall){
   $result = " ";
   $approval =0;
   $stmt=$conn ->prepare("SELECT * FROM short_distance WHERE hall_id =:id AND approval =:r");
   $stmt->bindParam(":id",$hall);
   $stmt->bindParam(":r",$approval);
   $stmt ->execute(); 
   $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $lid =$row['s_distance_id'];
    $d_of_booking = $row['d_of_booking'];
    $sid =$row['student_id'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric = $student['matric_no'];
    $room  =   $student['room_no'];


    $result .='  <tr>
        <td>'. $d_of_booking .'</td>

        <td>'.$name.'</td>
        <td>'.$matric.'</td>
        <td>'.$room.'</td>

      </tr>
      ';
}
return $result;
}
}

function getDisapprovedLong($conn,$hall){
   $result = " ";
   $approval =0;
   $stmt=$conn ->prepare("SELECT * FROM short_distance WHERE hall_id =:id AND approval =:r");
   $stmt->bindParam(":id",$hall);
   $stmt->bindParam(":r",$approval);
   $stmt ->execute(); 
   $rownum =  $stmt->rowCount();


  if($rownum>0){
  while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
    $lid =$row['s_distance_id'];
    $d_of_booking = $row['d_of_booking'];
    $sid =$row['student_id'];
    $student =   findStudentDetails($conn,$sid);
    $name  =   $student['fname']. ' '. $student['lname'];
    $matric = $student['matric_no'];
    $room  =   $student['room_no'];


    $result .='  <tr>
        <td>'. $d_of_booking .'</td>

        <td>'.$name.'</td>
        <td>'.$matric.'</td>
        <td>'.$room.'</td>

      </tr>
      ';
}
return $result;
}
}







   
?>
