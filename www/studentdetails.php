<?php
include("includes/connections.php");
include("includes/functions.php");

if(isset($_GET['sid']) && isset($_GET['lid']))
{
  $id =  $_GET['sid'] ;
  $lid =  $_GET['lid'] ;
}
chkAdminLogin();

?>


<!DOCTYPE html>
<html>
<head>
	<title>admin modal</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="admin">


<!-- FLEXIBLE MENU BUTTON -->
	<div class="menu-btn">
		<div class="b-bars b1"></div>
		<div class="b-bars b2"></div>
		<div class="b-bars b3"></div>
	</div>

<!-- BACKGROUND HEADER WITH LOGO AND TEXT -->
	<div class="background-header">
		<div class="overlay"></div>
		<div class="logo"></div>
		<h1 class="module-name">Student Details</h1>
	</div>

<!-- MODAL MENU FOR NAVIGATION -->
	<div class="menu-modal">
		<div class="dialog">
			<button class="def-button trigger-btn">What do you want to do?</button>
			<ul class="options-list">
			<a href="approved.php"><li class="option">Approved Booking</li></a>
			<a href="disapproved.php"><li class="option">Disapproved Booking</li></a>
			<a href="submitted_long.php"><li class="option">Submitted Long Distance</li></a>
			<a href="submitted_short.php"><li class="option">Submitted Short Distance</li></a>
			<a href="logout.php"><li class="option">Sign Out</li></a>


			</ul>
		</div>
	</div>

	<div class="admin-modal">
		<div class="modal-content">
			<?php
			$a = getStudentDetailsLong($conn,$id);
			echo $a;
			?>
			<form class="approval-form" action="approve.php?lid=<?php echo $lid ?>" method="POST">
		<a href="approve.php?lid=<?php echo $lid ?>"><button class="approve def-button">Approve</button></a>
				<textarea class="reason" name ="reason" placeholder="reason"></textarea>
			<a href="disapprove.php?lid=<?php echo $lid ?>">	<button class="disapprove def-button"name ="disapprove" type ="submit ">Disaprove</button>
			</form>
		</div>
	</div>

<script type="text/javascript" src="js/pass.js"></script>
</body
</html>
