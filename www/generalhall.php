
<?php
//session_start();
include("includes/connections.php");
include("includes/functions.php");

$errors =[];
if(array_key_exists('submit', $_POST))
{

	if(!empty($_POST['hall']))
	{
		$h = $_POST['hall'];
	}
	else
	{
		$errors['hall'] ="Please enter the hall";
	}

	if(empty($errors))
	{
		
		
		registerHall($conn,$h);
	
   }else{
   	var_dump($errors);
   }
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>long distance</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="long-distance">

<!-- FLEXIBLE MENU BUTTON -->
	<div class="menu-btn">
		<div class="b-bars b1"></div>
		<div class="b-bars b2"></div>
		<div class="b-bars b3"></div>
	</div>

<!-- BACKGROUND HEADER WITH LOGO AND TEXT -->
	<div class="background-header">
		<div class="overlay"></div>
		<div class="logo"></div>
		<h1 class="module-name">Home</h1>
	</div>

<!-- MODAL MENU FOR NAVIGATION -->
	<div class="menu-modal">
		<div class="dialog">
			<button class="def-button trigger-btn">What do you want to do?</button>
			<ul class="options-list">
				<a href="admin_register.php"><li class="option">Add Hall Admin</li></a>
				<a href="generalhall.php"><li class="option">Add Hall</li></a>

				
			</ul>
		</div>
	</div>

<!-- LONG DISTANCE BOOKING FORM -->
	<form class="def-form long-dist-form clearfix" action =""  method ="POST" >
		<input type="text" name="hall" class="text-field" placeholder="Hall Name">
		<input type="submit" name="submit" class="text-field" value="Apply">
	</form>

<script type="text/javascript" src="js/pass.js">
</script>
</body>
</html>
