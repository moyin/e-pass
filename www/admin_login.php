<?php
session_start();
include("includes/connections.php");

# including function file
include("includes/functions.php");

#store error
$errors = [];

if(array_key_exists('login', $_POST)){
	if(!empty($_POST['email'])){
		$e = $_POST['email'];
	}
	else{
		$errors["email"] = "please enter the name";
	}

	if(!empty($_POST['password'])){
		$p = $_POST['password'];
	}
	else{
		$errors["password"] = "please enter the password";
	}
	if(empty($errors)){
       # we go to db
      doAdminLogin($conn,$e, $p);
	}
}



?>


<!DOCTYPE html>
<html>
<head>
	<title>login</title>
	<link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body id="login">

<!-- BACKGROUND IMAGE AND OVERLAY -->
	<div class="background">
		<div class="overlay"></div>
		<div class="img"></div>
	</div>

<!-- TOP BAR WITH LOGO AND TEXT -->
	<div class="top-bar">
		<div class="logo"></div>
		<h1>Babcock University Pass Booking</h1>
	</div>

<!-- LOGIN FORM -->
	<form class="def-form login-form clearfix" action=" " method = "POST">
		<?php if(isset($_GET['msg'])){ echo'<span class="err">'. $_GET['msg'].'</span>';}?> 
<div>
		<label for="login-form" class="header">Sign In</label>

		<?php printError('email', $errors); ?>
		<input type="text" name="email" placeholder="email" class="text-field">

		<?php  printError('password', $errors); ?>
		<input type="password" name="password" placeholder="Password" class="text-field">

		<input type="submit" name="login" value="Sign In" class="text-field">
	</form>

<script type="text/javascript" src="js/index.js"></script>
</body>
</html>
