<?php
session_start();

 $hall  = $_SESSION['hall'] ;
 

include("includes/header.php");
include("includes/functions.php");
include("includes/connections.php");
chkAdminLogin();
?>

<!-- NAVIGATION DIALOGUE BOX -->
	<div class="dialog">
		<button class="def-button trigger-btn">What do you want to do?</button>
		<ul class="options-list">
			<a href="approved_long.php?hall=<?php echo $hall ?>"><li class="option">Approved Long Booking</li></a>
			<a href="approved_short.php?hall=<?php echo $hall ?>"><li class="option">Approved Short Booking</li></a>
			<a href="disapproved.php"><li class="option">Disapproved Booking</li></a>
			<a href="submitted_long.php"><li class="option">Submitted Long Distance</li></a>
			<a href="submitted_short.php"><li class="option">Submitted Short Distance</li></a>
			<a href="adminlogout.php"><li class="option">Sign Out</li></a>
		
			</ul>
		</div>
	</div>


	<button class="disapprove def-button" >Close Portal</button>
<?php
include("includes/footer.php");

?>
